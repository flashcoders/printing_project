var mongoose = require("mongoose"),
    User = require("./user");

var postSchema = new mongoose.Schema({
    code:String,
    subject:String,
    title:String,
    fileName:Number,
    created:  {type: Date, default: Date.now},
    author:{
        id:{
            type:mongoose.Schema.Types.ObjectId,
            rel:User
        },
        username:String
    },
    comments:[
            {
                    type:mongoose.Schema.Types.ObjectId,
                    ref:"comment"
                }
        ]
});

module.exports = mongoose.model("post",postSchema);