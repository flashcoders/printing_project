var express = require("express"),
    Post = require("../post"),
    Comment = require("../comment"),
    multer = require("multer"),
    bodyParser = require("body-parser"),
    path = require("path"),
    router = express.Router();
    
var filenameIndex = 1;
    
router.use(bodyParser.urlencoded({extended: true}));    
            //multer storage
            var storage = multer.diskStorage({
                destination: function (req, file, cb) {
                    cb(null, 'uploads/');
                },
                filename: function (req, file, cb) {
                    cb(null, filenameIndex+path.extname(file.originalname));
                }
            });


            var upload = multer({storage:storage}).single("fileUpload");

router.get("/", function(req, res){
    Post.find({},function (err,posts) {
        if (err) {
            console.log(err);
        } else {
            res.render("index",{posts:posts,currentUser:req.user});
        }
    });
    
});

router.get("/new",isAdimin, function(req, res){
   res.render("new"); 
});

router.post("/",isAdimin, function(req, res){

            upload(req, res, function (err) {
                if (err) {
                    console.log(err);
                    res.redirect("/home");
                    }else{
                        var createdPost = req.body.post;
                        console.log(filenameIndex);
                        createdPost.fileName = filenameIndex;
                        createdPost.author = {
                        id:req.user._id,
                        username:req.user.username
                        };
                        console.log(createdPost);
                        Post.create(createdPost,function (err,savedPost) {
                            if (err) {
                                console.log(err);
                                res.redirect("/home");
                            }else{
                                console.log("file uploaded successfully");
                                filenameIndex++;
                                res.redirect("/home");
                            }
                        });
                    }
            });
});
router.get("/:id",isLoggedIn ,function(req, res){
    Post.findById(req.params.id).populate("comments").exec(function (err,resultPost) {
        if (err) {
           console.log(err);
       }else{
           res.render("show",{resultPost:resultPost});
       }
    });
});

// downloading route
router.get("/:id/download",function (req,res) {
    Post.findById(req.params.id,function (err,post) {
        if (err) {
            console.log("error");
            console.log(err);
            res.redirect("back");
        } else {
            console.log(post);
            res.download("./uploads/"+post.fileName+".pdf");   
        }
    });
});

function isLoggedIn(req,res,next) {
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/home/#login");
}


function isAdimin(req,res,next) {
    if(req.isAuthenticated()){
        if(req.user.isAdmin){
            return next();
    }}
    res.send("only admin can do this");
}

module.exports = router;