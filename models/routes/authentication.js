var express = require("express"),
    passport = require("passport"),
    User = require("../user"),
    router = express.Router();


router.get("/", function(req, res){
    res.redirect("/home");
});
//AUTHENTICATION ROUTES

router.get("/register",function(req, res) {
    res.render("register",{currentUser:req.user});
});

router.post("/register",function(req, res) {
    var newUser = new User({username:req.body.username});
    User.register(newUser,req.body.password,function (err,user) {
        if (err) {
            console.log(err);
            return res.render("register");
        } 
            passport.authenticate("local")(req,res,function () {
                res.redirect("/home");
            });
    });
});
router.get("/login",function(req, res) {
  res.render("login",{currentUser:req.user}); 
});


router.post("/login",passport.authenticate("local",{
    successRedirect:"back",
    failureRedirect:"/"
}) ,function(req, res) {
    
});

router.get("/logout",function(req, res) {
    req.logout();
    res.redirect("/");
});

module.exports = router;