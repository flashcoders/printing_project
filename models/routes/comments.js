var express = require("express"),
    Post = require("../post"),
    Comment = require("../comment"),
    Blog = require("../blog"),
    router = express.Router();

//Notes comments
// router.get("/home/:id/comment/new",isLoggedIn,function (req,res) {
//     Post.findById(req.params.id,function (err,post) {
//         if (err) {
//             console.log(err);
//         } else {
//             res.render("newComment",{post:post});
//         }
//     });
    
// });

router.post("/home/:id/comment",isLoggedIn,function(req, res) {
   Comment.create(req.body.comment,function (err,comment) {
        if (err) {
            console.log(err);
        } else {
            Post.findById(req.params.id,function(err, post) {
                if (err) {
                    console.log(err);
                } else {
               var author = {id:req.user._id,
                        username:req.user.username
                       };
                        comment.author = author;
                        comment.save();
                        post.comments.push(comment);
                        post.save();
                        res.redirect("/home/"+req.params.id);     
                }
                        
            });
        }
   }); 
});

router.get("/home/:id/comment/:comment_id/edit",Authorisation,function (req,res) {
   Comment.findById(req.params.comment_id,function(err, comment) {
       if (err) {
           console.log(err);
       } else {
           res.render("editComment",{comment:comment,post_id:req.params.id});
       }
   }) ;
});

router.put("/home/:id/comment/:comment_id",Authorisation,function (req,res) {
   Comment.findByIdAndUpdate(req.params.comment_id,req.body.comment,function (err,updatedComment) {
       if (err) {
           console.log(err);
       } else {
           res.redirect("/home/"+req.params.id);
       }
   });
});

router.delete("/home/:id/comment/:comment_id",Authorisation,function (req,res) {
   Comment.findByIdAndRemove(req.params.comment_id,function (err) {
       if (err) {
           console.log(err);
       } else {
           res.redirect("/home/"+req.params.id);
       }
   }) ;
});

//Blog comments

// router.get("/blog/:id/comment/new",isLoggedIn,function (req,res) {
//     Blog.findById(req.params.id,function (err,blog) {
//         if (err) {
//             console.log(err);
//         } else {
//             res.render("newComment",{post:blog});
//         }
//     });
    
// });

router.post("/blogs/:id/comment",isLoggedIn,function(req, res) {
   Comment.create(req.body.comment,function (err,comment) {
        if (err) {
            console.log(err);
        } else {
            Blog.findById(req.params.id,function(err, blog) {
                if (err) {
                    console.log(err);
                } else {
               var author = {id:req.user._id,
                        username:req.user.username
                       };
                        comment.author = author;
                        comment.save();
                        blog.comments.push(comment);
                        blog.save();
                        res.redirect("/blogs/"+req.params.id);     
                }
                        
            });
        }
   }); 
});

router.get("/blogs/:id/comment/:comment_id/edit",Authorisation,function (req,res) {
   Comment.findById(req.params.comment_id,function(err, comment) {
       if (err) {
           console.log(err);
       } else {
           res.render("editComment_blog",{comment:comment,post_id:req.params.id});
       }
   }) ;
});

router.put("/blogs/:id/comment/:comment_id",Authorisation,function (req,res) {
   Comment.findByIdAndUpdate(req.params.comment_id,req.body.comment,function (err,updatedComment) {
       if (err) {
           console.log(err);
       } else {
           res.redirect("/blogs/"+req.params.id);
       }
   });
});

router.delete("/blogs/:id/comment/:comment_id",Authorisation,function (req,res) {
   Comment.findByIdAndRemove(req.params.comment_id,function (err) {
       if (err) {
           console.log(err);
       } else {
           res.redirect("/blogs/"+req.params.id);
       }
   }) ;
});



function Authorisation(req,res,next) {
    if (req.isAuthenticated()) {
        Comment.findById(req.params.comment_id,function(err, foundComment) {
            if(err){
                res.redirect("back");
            }
            if (foundComment.author.id.equals(req.user._id)) {
                return next();
            } else {
                res.redirect("back");
            }
        });
    } else {
        res.send("you need to be logged in");
    }
}
function isLoggedIn(req,res,next) {
    if(req.isAuthenticated()){
        return next();
    }
    res.send("you need to be logged in");
}

module.exports = router;